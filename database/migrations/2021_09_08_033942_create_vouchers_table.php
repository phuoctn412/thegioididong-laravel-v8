<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('vouchers', function (Blueprint $table) {
      $table->id();
      $table->string('title');
      $table->string('code'); // voucher's code
      $table->boolean('is_active')->default(true);
      $table->integer('discount');
      $table->integer('min_price'); // min price to apply voucher
      $table->integer('quantity');
      $table->date('date_start');
      $table->date('date_end');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('vouchers');
  }
}
