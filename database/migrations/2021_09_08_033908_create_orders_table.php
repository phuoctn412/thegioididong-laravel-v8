<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('orders', function (Blueprint $table) {
      $table->id();
      $table->unsignedBigInteger('user_id')->nullable(); // nullable for guest(visitor)
      $table->foreign('user_id')->references('id')->on('users');
      $table->string('name');
      $table->integer('phone');
      $table->string('address');
      $table->integer('total_price');
      $table->date('date_start')->nullable();
      $table->date('date_end')->nullable();
      $table->tinyInteger('status')->default(1);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('orders');
  }
}
