<?php

namespace Database\Seeders;

use App\Models\Storage;
use Illuminate\Database\Seeder;

class StorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::insert([
            ['storage' => '64GB'],
            ['storage' => '128GB'],
            ['storage' => '256GB'],
            ['storage' => '512GB'],
            ['storage' => '1TB'],
            ['storage' => '2TB'],
            ['storage' => '4TB']
        ]);
    }
}
