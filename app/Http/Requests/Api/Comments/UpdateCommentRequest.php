<?php

namespace App\Http\Requests\Api\Comments;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating_id' => 'required|numeric',
            'comment_id' => 'required|numeric',
            'point' => 'required|numeric|min:1|max:5',
            'content' => 'required|string',
            'total_price' => 'required|numeric'
        ];
    }
}
