<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Role;

class EnsureRoleIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->role_id == Role::SUPER_ADMIN_ROLE) {
            return $next($request);
        } else {
            return response()->json(['message' => 'Permission denied!'], 403);
        }
    }
}
