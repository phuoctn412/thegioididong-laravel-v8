<?php

namespace App\Http\Controllers\apis\Comments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Comments\CreateCommentRequest;
use App\Http\Requests\Api\Comments\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Rating;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function show($id)
    {
        $query = DB::table('comments')
            ->join('ratings', 'comments.product_id', '=', 'ratings.product_id')
            ->join('users', 'ratings.user_id', '=', 'users.id')
            ->select('comments.content','ratings.point', 'users.email')
            ->get();
        return response()->json($query);
    }

    public function getCommentByProduct($productID)
    {
        return response()->json(Comment::with(['user'])->where('product_id', $productID)->get());
    }

    public function getCommentDetail($userId, $productId)
    {
        $rating = Rating::where('user_id', $userId)->where('product_id', $productId)->first();
        $comment = Comment::where('user_id', $userId)->where('product_id', $productId)->first();
        $data = [
            'rating' => $rating,
            'comment' => $comment,
        ];
        return response()->json([$data], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(CreateCommentRequest $request)
    {
        $data = $request->only(['user_id', 'product_id', 'parent_comment_id', 'content']);
        DB::beginTransaction();

        try {
            Comment::create($data);
            Rating::create([
                'user_id' => $request->input('user_id'),
                'product_id' => $request->input('product_id'),
                'point' => $request->input('point'),
            ]);
            DB::commit();

            return response()->json(['message' => 'comment success'], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response($message);
    }


    public function updateCommentAndRating(UpdateCommentRequest $request)
    {
        DB::beginTransaction();

        try {
            Rating::where('id', $request->input('ratingId'))->update(['point' => $request->input('point')]);
            Comment::where('id', $request->input('commentId'))->update(['content' => $request->input('content')]);

            DB::commit();

            $message = 'Update successfully';
            return response()->json(['success' => $message], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        return response($message);
    }
}
