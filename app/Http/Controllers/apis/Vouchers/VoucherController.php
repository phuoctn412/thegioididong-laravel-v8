<?php

namespace App\Http\Controllers\apis\Vouchers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admins\Voucher\CreateVoucherRequest;
use App\Models\Voucher;
use App\Http\Controllers\Vouchers\VoucherController as VoucherControllerExtend;

class VoucherController extends Controller
{
    public function getCurrentVoucher($voucherCode): \Illuminate\Http\JsonResponse
    {
        return response()->json(VoucherControllerExtend::getCurrentVoucher($voucherCode), 200);
    }

    public function store(CreateVoucherRequest $request): \Illuminate\Http\JsonResponse
    {
        Voucher::create($request->only(['title', 'code', 'discount', 'date_start', 'date_end', 'min_price', 'quantity']));
        return response()->json('Create voucher success', 200);
    }

    public function checkVoucherIsInvalid($voucherCode): \Illuminate\Http\JsonResponse
    {
        return response()->json(['isInvalid' => VoucherControllerExtend::checkInvalidVoucher($voucherCode)]);
    }

    public function getVoucherWithPrice($price): \Illuminate\Http\JsonResponse
    {
        return response()->json(Voucher::whereRaw('is_active = true and min_price <= ?', [$price])->get());
    }
}
