<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{

    use HasFactory;

    protected $fillable = ['title', 'code', 'is_active', 'date_start', 'date_end', 'discount', 'min_price', 'quantity'];
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];
}
