<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'name', 'phone', 'address', 'date_start', 'date_end', 'status', 'total_price'];

    const PENDING_STATUS = 1;
    const APPROVE_STATUS = 2;
    const CANCEL_STATUS = 3;

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function voucherDetails()
    {
        return $this->hasMany(VoucherDetail::class);
    }
}
