<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apis\Comments\CommentController;

Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::post('/products/{id}/comments', [CommentController::class, 'store']);
    Route::get('/users/{userId}/products/{productId}/comments', [CommentController::class, 'getCommentDetail']);
    Route::put('/comments/ratings/update', [CommentController::class, 'updateCommentAndRating']);
});

Route::get('/products/{id}/comments', [CommentController::class, 'show']);
