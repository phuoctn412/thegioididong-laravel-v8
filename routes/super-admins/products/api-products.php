<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apis\Products\ProductController;

Route::group(['middleware' => ['auth:sanctum', 'auth.admin']], function () {
    Route::post('/products', [ProductController::class, 'store']); // admin create product
    // Route::put('/products/{id}', [ProductController::class, 'update']);
    // Route::delete('/products/{id}', [ProductController::class, 'destroy']);
});
