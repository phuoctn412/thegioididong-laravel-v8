<?php

use App\Http\Controllers\apis\Orders\OrderController;
use App\Http\Controllers\apis\Vouchers\VoucherController;
use Illuminate\Support\Facades\Route;

Route::group(
    ['middleware' => ['auth:sanctum', 'throttle:global']],
    function () {
        Route::post('admins/vouchers', [VoucherController::class, 'store']);
        Route::get('/users/{id}/orders', [OrderController::class, 'getOrders']);
        Route::get('/orders/{id}', [OrderController::class, 'getOrderDetails']);
	Route::get('/admins/orders', [OrderController::Class, 'index']);
    }
);
Route::post('orders', [OrderController::class, 'store']);
Route::get('is-invalid-voucher/{voucherCode}', [VoucherController::class, 'checkVoucherIsInvalid']);
Route::get('vouchers/{voucherCode}', [VoucherController::class, 'getCurrentVoucher']);
Route::get('vouchers-with-price/{price}', [VoucherController::class, 'getVoucherWithPrice']); // get voucher with available with order's total price
